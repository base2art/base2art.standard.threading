namespace Base2art.Threading
{
    using System;

    /// <summary>
    ///     A class that implements <see cref="ILazy{T}" />,
    ///     When the function invocation throws an exception this class will throw the exception one time
    ///     and retry on subsequent invocations.
    /// </summary>
    public class RetryLazy<T> : ILazy<T>
    {
        private readonly Func<T> function;

        private readonly object padLock = new object();

        private T value;

        /// <summary>
        ///     Creates a new instance of the <see cref="OneTryLazy{T}" /> class.
        /// </summary>
        /// <param name="function">The backing function.</param>
        public RetryLazy(Func<T> function)
        {
            this.function = function ?? throw new ArgumentNullException(nameof(function));
        }

        /// <summary>
        ///     Gets the item from the backing source.
        /// </summary>
        public T Value
        {
            get
            {
                if (!this.IsValueCreated)
                {
                    lock (this.padLock)
                    {
                        if (!this.IsValueCreated)
                        {
                            this.value = this.function();
                            this.IsValueCreated = true;
                        }
                    }
                }

                return this.value;
            }
        }

        /// <summary>
        ///     Gets a value indicating that the lazy value has been created.
        /// </summary>
        public bool IsValueCreated { get; private set; }

        /// <summary>
        ///     Calling this method will allow you to get the value again but without a cache.
        /// </summary>
        public void Reset()
        {
            this.IsValueCreated = false;
        }
    }
}