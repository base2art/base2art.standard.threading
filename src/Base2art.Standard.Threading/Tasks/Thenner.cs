﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    /// <summary>
    ///     A set of extension methods that affect <see cref="Task" /> and <see cref="Task{TResult}" />.
    /// </summary>
    public static class Thenner
    {
        /// <summary>
        ///     A method to provide continuation.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The wrapper of the mapper.</returns>
        public static TaskThenner Then(this Task input)
        {
            return new TaskThenner(input);
        }

        /// <summary>
        ///     A method to provide continuation.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The wrapper of the mapper.</returns>
        public static ObjectThenner<TIn> Then<TIn>(this Task<TIn> input)
        {
            return new ObjectThenner<TIn>(input);
        }

        /// <summary>
        ///     A method to provide continuation.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The wrapper of the mapper.</returns>
        public static CollectionThenner<List<TIn>, TIn> Then<TIn>(this Task<List<TIn>> input)
        {
            return new CollectionThenner<List<TIn>, TIn>(input);
        }

        /// <summary>
        ///     A method to provide continuation.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The wrapper of the mapper.</returns>
        public static CollectionThenner<TIn[], TIn> Then<TIn>(this Task<TIn[]> input)
        {
            return new CollectionThenner<TIn[], TIn>(input);
        }

        /// <summary>
        ///     A method to provide continuation.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The wrapper of the mapper.</returns>
        public static CollectionThenner<IEnumerable<TIn>, TIn> Then<TIn>(this Task<IEnumerable<TIn>> input)
        {
            return new CollectionThenner<IEnumerable<TIn>, TIn>(input);
        }

        /// <summary>
        ///     A method to provide task continuation with exception handling and completion events.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The task wrapper.</returns>
        public static ExceptionalTaskThenner WithEvents(this Task input)
        {
            return new ExceptionalTaskThenner(input);
        }

        /// <summary>
        ///     A method to provide task continuation with exception handling and completion events.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The task wrapper.</returns>
        public static ExceptionalObjectThenner<TIn> WithEvents<TIn>(this Task<TIn> input)
        {
            return new ExceptionalObjectThenner<TIn>(input);
        }

        /// <summary>
        ///     A method to provide task continuation with exception handling and completion events.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The task wrapper.</returns>
        public static ExceptionalFuncTaskThenner WithEvents(this Func<Task> input)
        {
            return new ExceptionalFuncTaskThenner(input);
        }

        /// <summary>
        ///     A method to provide task continuation with exception handling and completion events.
        /// </summary>
        /// <param name="input">The initial task.</param>
        /// <returns>The task wrapper.</returns>
        public static ExceptionalFuncObjectThenner<TIn> WithEvents<TIn>(this Func<Task<TIn>> input)
        {
            return new ExceptionalFuncObjectThenner<TIn>(input);
        }
    }
}