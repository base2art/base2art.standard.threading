﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    ///     The wrapper for the task extension library for accomodating exceptional events.
    /// </summary>
    /// <typeparam name="TIn">The Task's return type.</typeparam>
    public class ExceptionalObjectThenner<TIn>
    {
        private Func<Exception, Task> exceptionRunnerBacking;
        private Func<TIn, Task> successRunnerBacking;

        /// <summary>
        ///     Creates a new instance of the <see cref="ExceptionalObjectThenner{T}" /> class.
        /// </summary>
        /// <param name="task">The backing action.</param>
        public ExceptionalObjectThenner(Task<TIn> task)
        {
            this.FunctionalTask = task;
        }

        /// <summary>
        ///     Gets the initial task to run.
        /// </summary>
        protected Task<TIn> FunctionalTask { get; }

        /// <summary>
        ///     The action to execute on when the task throws an exception.
        /// </summary>
        /// <param name="exceptionRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalObjectThenner<TIn> OnError(Func<Exception, Task> exceptionRunner)
        {
            this.exceptionRunnerBacking = exceptionRunner;
            return this;
        }

        /// <summary>
        ///     The action to execute on when the task throws an exception.
        /// </summary>
        /// <param name="exceptionRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalObjectThenner<TIn> OnError(Action<Exception> exceptionRunner)
        {
            this.exceptionRunnerBacking = ex =>
            {
                exceptionRunner(ex);
                return Task.FromResult(true);
            };

            return this;
        }

        /// <summary>
        ///     The action to execute on when the task executes sucessfully.
        /// </summary>
        /// <param name="successRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalObjectThenner<TIn> OnCompleted(Func<TIn, Task> successRunner)
        {
            this.successRunnerBacking = successRunner;
            return this;
        }

        /// <summary>
        ///     The action to execute on when the task executes sucessfully.
        /// </summary>
        /// <param name="successRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalObjectThenner<TIn> OnCompleted(Action<TIn> successRunner)
        {
            this.successRunnerBacking = ex =>
            {
                successRunner(ex);
                return Task.FromResult(true);
            };
            return this;
        }

        /// <summary>
        ///     Executes the task.
        /// </summary>
        /// <returns>The task.</returns>
        public Task<TIn> Execute()
        {
            var runner = new TaskExceptionRunner<TIn>(
                                                      () => this.FunctionalTask,
                                                      this.exceptionRunnerBacking,
                                                      this.successRunnerBacking);

            return runner.Retry(0, 0);
        }
    }
}