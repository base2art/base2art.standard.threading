﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    ///     A set of extension methods that affect <see cref="Task{TResult}" />
    /// </summary>
    /// <typeparam name="TIn">The type of element returned by the task.</typeparam>
    public class ObjectThenner<TIn> : TaskThenner
    {
        /// <summary>
        ///     Creates a new instance of the <see cref="OneTryEnsurer" /> class.
        /// </summary>
        /// <param name="functionalTask">The backing task.</param>
        public ObjectThenner(Task<TIn> functionalTask)
            : base(functionalTask)
        {
            this.FunctionalTask = functionalTask;
        }

        /// <summary>
        ///     Gets the task that returns a value.
        /// </summary>
        protected Task<TIn> FunctionalTask { get; }

        /// <summary>
        ///     Maps the task return object to a new object via the <paramref name="map" />.
        /// </summary>
        /// <param name="map">The mapping function.</param>
        /// <typeparam name="TResult">The type returned by the mapping function.</typeparam>
        /// <returns>The return value.</returns>
        public async Task<TResult> Return<TResult>(Func<TIn, TResult> map)
        {
            var rez = await this.FunctionalTask;
            return rez == null ? default(TResult) : map(rez);
        }

        /// <summary>
        ///     Maps the task return object to a new object via the <paramref name="map" />.
        /// </summary>
        /// <param name="map">The mapping function.</param>
        /// <typeparam name="TResult">The type returned by the mapping function.</typeparam>
        /// <returns>The return value.</returns>
        public async Task<TResult> Return<TResult>(Func<TIn, Task<TResult>> map)
        {
            var rez = await this.FunctionalTask;
            return rez == null ? default(TResult) : await map(rez);
        }

        /// <summary>
        ///     Maps the task return object to a new object.
        /// </summary>
        /// <param name="result">The value to return if the task returns null.</param>
        /// <returns>A non null value.</returns>
        public Task<TIn> Coalesce(TIn result)
        {
            return this.Coalesce(() => result);
        }

        /// <summary>
        ///     Maps the task return object to a new object.
        /// </summary>
        /// <param name="map">A function that returns a value if the task returns null.</param>
        /// <returns>A non null value.</returns>
        public async Task<TIn> Coalesce(Func<TIn> map)
        {
            var rez = await this.FunctionalTask;
            if (rez == null)
            {
                return map();
            }

            return rez;
        }

        /// <summary>
        ///     Maps the task return object to a new object.
        /// </summary>
        /// <param name="map">A function that returns a value if the task returns null.</param>
        /// <returns>A non null value.</returns>
        public async Task<TIn> Coalesce(Func<Task<TIn>> map)
        {
            var rez = await this.FunctionalTask;
            if (rez == null)
            {
                return await map();
            }

            return rez;
        }

        /// <summary>
        ///     After the initial task completes executes a function via the <paramref name="nextAction" />.
        /// </summary>
        /// <param name="nextAction">The next task producer.</param>
        /// <returns>The task.</returns>
        public virtual async Task Execute(Action<TIn> nextAction)
        {
            var value = await this.FunctionalTask;
            nextAction(value);
        }
    }
}