﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    ///     The wrapper for the task extension library for accomodating exceptional events.
    /// </summary>
    public class ExceptionalFuncTaskThenner
    {
        private Func<Exception, Task> exceptionRunnerBacking;
        private Func<Task> successRunnerBacking;

        /// <summary>
        ///     Creates a new instance of the <see cref="ExceptionalFuncTaskThenner" /> class.
        /// </summary>
        /// <param name="task">The backing action.</param>
        public ExceptionalFuncTaskThenner(Func<Task> task)
        {
            this.ActionTask = task;
        }

        /// <summary>
        ///     Gets the task to run.
        /// </summary>
        protected Func<Task> ActionTask { get; }

        /// <summary>
        ///     The action to execute on when the task throws an exception.
        /// </summary>
        /// <param name="exceptionRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncTaskThenner OnError(Func<Exception, Task> exceptionRunner)
        {
            this.exceptionRunnerBacking = exceptionRunner;
            return this;
        }

        /// <summary>
        ///     The action to execute on when the task throws an exception.
        /// </summary>
        /// <param name="exceptionRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncTaskThenner OnError(Action<Exception> exceptionRunner)
        {
            this.exceptionRunnerBacking = ex =>
            {
                exceptionRunner(ex);
                return Task.FromResult(false);
            };

            return this;
        }

        /// <summary>
        ///     The action to execute on when the task executes sucessfully.
        /// </summary>
        /// <param name="successRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncTaskThenner OnCompleted(Func<Task> successRunner)
        {
            this.successRunnerBacking = successRunner;
            return this;
        }

        /// <summary>
        ///     The action to execute on when the task executes sucessfully.
        /// </summary>
        /// <param name="successRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncTaskThenner OnCompleted(Action successRunner)
        {
            this.successRunnerBacking = () =>
            {
                successRunner();
                return Task.FromResult(false);
            };

            return this;
        }

        /// <summary>
        ///     Executes the task.
        /// </summary>
        /// <returns>The task.</returns>
        public Task Execute()
        {
            var runner = new TaskExceptionRunner<bool>(
                                                       () => this.ActionTask().Then().Return(true),
                                                       this.exceptionRunnerBacking,
                                                       x => this.successRunnerBacking());
            return runner.Retry(0, 0);
        }

        /// <summary>
        ///     Execute the task and if it fails, it re-executes <paramref name="times">times</paramref>.
        /// </summary>
        /// <param name="times">The number of retries.</param>
        /// <returns>The task.</returns>
        public Task ExecuteWithRetry(int times)
        {
            var runner = new TaskExceptionRunner<bool>(
                                                       () => this.ActionTask().Then().Return(true),
                                                       this.exceptionRunnerBacking,
                                                       this.successRunnerBacking == null
                                                           ? (Func<bool, Task>) null
                                                           : x => this.successRunnerBacking());
            return runner.Retry(0, times);
        }
    }
}