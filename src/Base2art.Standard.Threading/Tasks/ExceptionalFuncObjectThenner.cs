﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    ///     The wrapper for the task extension library for accomodating exceptional events.
    /// </summary>
    /// <typeparam name="TIn">The Task's return type.</typeparam>
    public class ExceptionalFuncObjectThenner<TIn>
    {
        private Func<Exception, Task> exceptionRunnerBacking;
        private Func<TIn, Task> successRunnerBacking;

        /// <summary>
        ///     Creates a new instance of the <see cref="ExceptionalFuncObjectThenner{TIn}" /> class.
        /// </summary>
        /// <param name="task">The backing action.</param>
        public ExceptionalFuncObjectThenner(Func<Task<TIn>> task)
        {
            this.FunctionalTask = task;
        }

        /// <summary>
        ///     Gets the task to run.
        /// </summary>
        protected Func<Task<TIn>> FunctionalTask { get; }

        /// <summary>
        ///     The action to execute on when the task throws an exception.
        /// </summary>
        /// <param name="exceptionRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncObjectThenner<TIn> OnError(Func<Exception, Task> exceptionRunner)
        {
            this.exceptionRunnerBacking = exceptionRunner;
            return this;
        }

        /// <summary>
        ///     The action to execute on when the task throws an exception.
        /// </summary>
        /// <param name="exceptionRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncObjectThenner<TIn> OnError(Action<Exception> exceptionRunner)
        {
            this.exceptionRunnerBacking = ex =>
            {
                exceptionRunner(ex);
                return Task.FromResult(true);
            };

            return this;
        }

        /// <summary>
        ///     The action to execute on when the task executes sucessfully.
        /// </summary>
        /// <param name="successRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncObjectThenner<TIn> OnCompleted(Func<TIn, Task> successRunner)
        {
            this.successRunnerBacking = successRunner;
            return this;
        }

        /// <summary>
        ///     The action to execute on when the task executes sucessfully.
        /// </summary>
        /// <param name="successRunner">The action to run.</param>
        /// <returns>The wrapping class.</returns>
        public ExceptionalFuncObjectThenner<TIn> OnCompleted(Action<TIn> successRunner)
        {
            this.successRunnerBacking = ex =>
            {
                successRunner(ex);
                return Task.FromResult(true);
            };
            return this;
        }

        /// <summary>
        ///     Executes the initial task.
        /// </summary>
        /// <returns>The task.</returns>
        public Task<TIn> Execute()
        {
            var runner = new TaskExceptionRunner<TIn>(
                                                      this.FunctionalTask,
                                                      this.exceptionRunnerBacking,
                                                      this.successRunnerBacking);

            return runner.Retry(0, 0);
        }

        /// <summary>
        ///     Execute the task and if it fails, it re-executes <paramref name="times">times</paramref>.
        /// </summary>
        /// <param name="times">The number of retries.</param>
        /// <returns>The task.</returns>
        public Task<TIn> ExecuteWithRetry(int times)
        {
            var runner = new TaskExceptionRunner<TIn>(
                                                      this.FunctionalTask,
                                                      this.exceptionRunnerBacking,
                                                      this.successRunnerBacking);

            return runner.Retry(0, times);
        }
    }
}