﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    ///     A set of extension methods that affect <see cref="Task{TResult}" />
    /// </summary>
    /// <typeparam name="TColl">The Type of collection</typeparam>
    /// <typeparam name="TElement">The type of element in the collection</typeparam>
    public class CollectionThenner<TColl, TElement> : ObjectThenner<TColl>
        where TColl : class, IEnumerable<TElement>
    {
        /// <summary>
        ///     Creates a new instance of the <see cref="CollectionThenner{TColl,TElement}" /> class.
        /// </summary>
        /// <param name="task">The backing action.</param>
        public CollectionThenner(Task<TColl> task)
            : base(task)
        {
        }

        /// <summary>
        ///     <![CDATA[
        /// Converts a Task<IEnumerable<T>> to Task<T[]>.
        /// ]]>
        /// </summary>
        /// <returns>A Task with an array of elements</returns>
        public Task<TElement[]> ToArray()
        {
            return this.Return(x => x == null ? null : x.ToArray());
        }

        /// <summary>
        ///     <![CDATA[
        /// Converts a Task<IEnumerable<T>> to Task<List<T>>.
        /// ]]>
        /// </summary>
        /// <returns>A Task with a list of elements</returns>
        public Task<List<TElement>> ToList()
        {
            return this.Return(x => x == null ? null : x.ToList());
        }

        /// <summary>
        ///     Returns the first element of a sequence in a task,
        ///     or a default value if the sequence contains no elements in the task.
        /// </summary>
        /// <returns>A Task with a single element or null.</returns>
        public Task<TElement> FirstOrDefault()
        {
            return this.Return(x => x == null ? default(TElement) : x.FirstOrDefault());
        }

        /// <summary>
        ///     <![CDATA[
        /// Maps items in Task<IEnumerable<T>> to Task<IEnumerable<TResult>>.
        /// ]]>
        /// </summary>
        /// <param name="map">The mapping function.</param>
        /// <typeparam name="TResult">The Result Type.</typeparam>
        /// <returns>A mapped <see cref="IEnumerable{T}" /> in a task.</returns>
        public async Task<IEnumerable<TResult>> Select<TResult>(Func<TElement, TResult> map)
        {
            var rez = await this.FunctionalTask;

            return rez == null ? null : rez.Select(map).ToArray();
        }

        /// <summary>
        ///     <![CDATA[
        /// Maps items in Task<IEnumerable<T>> to Task<IEnumerable<TResult>>.
        /// ]]>
        /// </summary>
        /// <param name="map">The mapping function.</param>
        /// <typeparam name="TResult">The Result Type.</typeparam>
        /// <returns>A mapped <see cref="IEnumerable{T}" /> in a task.</returns>
        public async Task<IEnumerable<TResult>> Select<TResult>(Func<TElement, Task<TResult>> map)
        {
            var rez = await this.FunctionalTask;
            if (rez == null)
            {
                return null;
            }

            return await Task.WhenAll(rez.Select(map));
        }

        /// <summary>
        ///     <![CDATA[
        /// Filters items in Task<IEnumerable<T>> to Task<IEnumerable<TResult>>.
        /// ]]>
        /// </summary>
        /// <param name="filter">The filtering function.</param>
        /// <returns>A mapped <see cref="IEnumerable{T}" /> in a task.</returns>
        public async Task<IEnumerable<TElement>> Where(Func<TElement, bool> filter)
        {
            var rez = await this.FunctionalTask;
            return rez == null ? null : rez.Where(filter);
        }

        /// <summary>
        ///     <![CDATA[
        /// Filters items in Task<IEnumerable<T>> to Task<IEnumerable<TResult>>.
        /// ]]>
        /// </summary>
        /// <param name="filter">The filtering function.</param>
        /// <returns>A mapped <see cref="IEnumerable{T}" /> in a task.</returns>
        public async Task<IEnumerable<TElement>> Where(Func<TElement, Task<bool>> filter)
        {
            var rez = await this.FunctionalTask;
            if (rez == null)
            {
                return null;
            }

            var items = new List<TElement>();
            foreach (var item in rez)
            {
                if (await filter(item))
                {
                    items.Add(item);
                }
            }

            return items;
        }
    }
}