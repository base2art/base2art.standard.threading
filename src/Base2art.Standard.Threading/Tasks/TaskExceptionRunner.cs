﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Threading.Tasks;

    internal class TaskExceptionRunner<TIn>
    {
        private readonly Func<Exception, Task> exceptionRunnerBacking;
        private readonly Func<Task<TIn>> functionalTask;
        private readonly Func<TIn, Task> successRunnerBacking;

        public TaskExceptionRunner(
            Func<Task<TIn>> functionalTask,
            Func<Exception, Task> exceptionRunnerBacking,
            Func<TIn, Task> successRunnerBacking)
        {
            this.functionalTask = functionalTask;
            this.exceptionRunnerBacking = exceptionRunnerBacking;
            this.successRunnerBacking = successRunnerBacking;
        }

        public async Task<TIn> Retry(int current, int times)
        {
            TIn result;
            try
            {
                result = await this.functionalTask();

                if (this.successRunnerBacking == null)
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                if (current != times)
                {
                    return await this.Retry(current + 1, times);
                }

                if (this.exceptionRunnerBacking != null)
                {
                    await this.exceptionRunnerBacking(ex);
                }

                throw;
            }

            await this.successRunnerBacking(result);
            return result;
        }
    }
}