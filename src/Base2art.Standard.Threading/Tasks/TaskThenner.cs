﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Threading.Tasks;

    /// <summary>
    ///     A set of extension methods that affect <see cref="Task{TResult}" />
    /// </summary>
    public class TaskThenner
    {
        /// <summary>
        ///     Creates a new instance of the <see cref="TaskThenner" /> class.
        /// </summary>
        /// <param name="task">The backing task.</param>
        public TaskThenner(Task task)
        {
            this.ActionTask = task;
        }

        /// <summary>
        ///     Gets the task that performs an <see cref="Action" />.
        /// </summary>
        protected Task ActionTask { get; }

        /// <summary>
        ///     After the initial task completes returns a new object.
        /// </summary>
        /// <param name="result">The result to return.</param>
        /// <typeparam name="TResult">The type returned by the mapping function.</typeparam>
        /// <returns>The return value.</returns>
        public virtual async Task<TResult> Return<TResult>(TResult result)
        {
            await this.ActionTask;
            return result;
        }

        /// <summary>
        ///     After the initial task completes returns a new object via the <paramref name="resultProducer" />.
        /// </summary>
        /// <param name="resultProducer">The mapping function.</param>
        /// <typeparam name="TResult">The type returned by the mapping function.</typeparam>
        /// <returns>The return value.</returns>
        public virtual async Task<TResult> Return<TResult>(Func<TResult> resultProducer)
        {
            await this.ActionTask;
            return resultProducer();
        }

        /// <summary>
        ///     After the initial task completes returns a new object via the <paramref name="resultProducer" />.
        /// </summary>
        /// <param name="resultProducer">The mapping function.</param>
        /// <typeparam name="TResult">The type returned by the mapping function.</typeparam>
        /// <returns>The return value.</returns>
        public virtual async Task<TResult> Return<TResult>(Func<Task<TResult>> resultProducer)
        {
            await this.ActionTask;
            return await resultProducer();
        }

        /// <summary>
        ///     After the initial task completes executes a new task via the <paramref name="resultProducer" />.
        /// </summary>
        /// <param name="resultProducer">The next task producer.</param>
        /// <returns>The task.</returns>
        public virtual async Task Execute(Func<Task> resultProducer)
        {
            await this.ActionTask;
            await resultProducer();
        }

        /// <summary>
        ///     After the initial task completes executes a function via the <paramref name="nextAction" />.
        /// </summary>
        /// <param name="nextAction">The next task producer.</param>
        /// <returns>The task.</returns>
        public virtual async Task Execute(Action nextAction)
        {
            await this.ActionTask;
            nextAction();
        }
    }
}