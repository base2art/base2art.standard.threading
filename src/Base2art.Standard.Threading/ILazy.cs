﻿namespace Base2art.Threading
{
    /// <summary>
    ///     An interface that represents a class that invokes a method to create a value one and only one time,
    ///     in a thread-safe manner.
    ///     Similar to the <see cref="System.Lazy{T}" />
    /// </summary>
    public interface ILazy<out T>
    {
        /// <summary>
        ///     Gets the item from the backing source.
        /// </summary>
        T Value { get; }

        /// <summary>
        ///     Gets a value indicating that the lazy value has been created.
        /// </summary>
        bool IsValueCreated { get; }

        /// <summary>
        ///     Calling this method will allow you to get the value again but without a cache.
        /// </summary>
        void Reset();
    }
}