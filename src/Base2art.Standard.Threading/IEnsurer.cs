﻿namespace Base2art.Threading
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     An interface that represents a class that invokes a method one and only one time, in a thread-safe manner.
    ///     Similar to the <see cref="System.Lazy{T}" />,
    ///     but with an <see cref="Action" /> instead of <see cref="Func{TResult}" />.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public interface IEnsurer
    {
        /// <summary>
        ///     Gets a value indicating that the ensurer has been invoked.
        /// </summary>
        bool Ensured { get; }

        /// <summary>
        ///     Ensures that the item is invoked.
        /// </summary>
        void Ensure();

        /// <summary>
        ///     Calling this method will allow you to invoke the item again.
        /// </summary>
        void Reset();
    }
}