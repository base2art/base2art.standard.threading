﻿namespace Base2art.Threading
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     A class that implements <see cref="ILazy{T}" />,
    ///     When the funcion invocation throws an exception all following invocations
    ///     are garanteed to also throw exceptions.
    /// </summary>
    public class OneTryLazy<T> : ILazy<T>
    {
        private readonly Func<T> function;

        private readonly object padLock = new object();

        private Exception exception;

        private T value;

        /// <summary>
        ///     Creates a new instance of the <see cref="OneTryLazy{T}" /> class.
        /// </summary>
        /// <param name="function">The backing function.</param>
        public OneTryLazy(Func<T> function)
        {
            this.function = function ?? throw new ArgumentNullException(nameof(function));
        }

        /// <summary>
        ///     Gets the item from the backing source.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public T Value
        {
            get
            {
                if (!this.IsValueCreated)
                {
                    lock (this.padLock)
                    {
                        if (!this.IsValueCreated)
                        {
                            try
                            {
                                this.value = this.function();
                            }
                            catch (Exception ex)
                            {
                                this.exception = ex;
                            }
                            finally
                            {
                                this.IsValueCreated = true;
                            }
                        }
                    }
                }

                if (this.exception != null)
                {
                    throw this.exception;
                }

                return this.value;
            }
        }

        /// <summary>
        ///     Gets a value indicating that the lazy value has been created.
        /// </summary>
        public bool IsValueCreated { get; private set; }

        /// <summary>
        ///     Calling this method will allow you to get the value again but without a cache.
        /// </summary>
        public void Reset()
        {
            this.exception = null;
            this.IsValueCreated = false;
        }
    }
}