﻿namespace Base2art.Threading
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     A class that implements <see cref="IEnsurer" />,
    ///     When the action invocation throws an exception this class will throw the exception one time
    ///     and retry on subsequent invocations.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public class RetryEnsurer : IEnsurer
    {
        private readonly Action action;

        private readonly object padLock = new object();

        /// <summary>
        ///     Creates a new instance of the <see cref="RetryEnsurer" /> class.
        /// </summary>
        /// <param name="action">The backing action.</param>
        public RetryEnsurer(Action action)
        {
            this.action = action ?? throw new ArgumentNullException(nameof(action));
        }

        /// <summary>
        ///     Gets a value indicating that the ensurer has been invoked.
        /// </summary>
        public bool Ensured { get; private set; }

        /// <summary>
        ///     Ensures that the item is invoked.
        /// </summary>
        public void Ensure()
        {
            if (!this.Ensured)
            {
                lock (this.padLock)
                {
                    if (!this.Ensured)
                    {
                        this.action();
                        this.Ensured = true;
                    }
                }
            }
        }

        /// <summary>
        ///     Calling this method will allow you to invoke the item again.
        /// </summary>
        public void Reset()
        {
            this.Ensured = false;
        }
    }
}