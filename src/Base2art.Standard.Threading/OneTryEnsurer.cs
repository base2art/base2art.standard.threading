﻿namespace Base2art.Threading
{
    using System;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>
    ///     A class that implements <see cref="IEnsurer" />,
    ///     When the action invocation throws an exception all following invocations
    ///     are garanteed to also throw exceptions.
    /// </summary>
    [SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Justification = "SjY")]
    public class OneTryEnsurer : IEnsurer
    {
        private readonly Action action;

        private readonly object padLock = new object();

        private Exception exception;

        /// <summary>
        ///     Creates a new instance of the <see cref="OneTryEnsurer" /> class.
        /// </summary>
        /// <param name="action">The backing action.</param>
        public OneTryEnsurer(Action action)
        {
            this.action = action ?? throw new ArgumentNullException(nameof(action));
        }

        /// <summary>
        ///     Gets a value indicating that the ensurer has been invoked.
        /// </summary>
        public bool Ensured { get; private set; }

        /// <summary>
        ///     Ensures that the item is invoked.
        /// </summary>
        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public void Ensure()
        {
            if (!this.Ensured)
            {
                lock (this.padLock)
                {
                    if (!this.Ensured)
                    {
                        try
                        {
                            this.action();
                        }
                        catch (Exception ex)
                        {
                            this.exception = ex;
                        }
                        finally
                        {
                            this.Ensured = true;
                        }
                    }
                }
            }

            if (this.exception != null)
            {
                throw this.exception;
            }
        }

        /// <summary>
        ///     Calling this method will allow you to invoke the item again.
        /// </summary>
        public void Reset()
        {
            this.exception = null;
            this.Ensured = false;
        }
    }
}