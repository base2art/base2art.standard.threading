using System.Diagnostics.CodeAnalysis;
using System.Reflection;

[assembly: AssemblyTitle("Base2art.Standard.BCL")]
[assembly: AssemblyDescription("Set of classes, modules and libraries for easier implementation of features.")]
[assembly: AssemblyConfiguration("")]

[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Collections", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.IO", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Net", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Security", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Serialization", Justification = "SjY")]
[assembly:
    SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace",
        Target = "Base2art.Text", Justification = "SjY")]