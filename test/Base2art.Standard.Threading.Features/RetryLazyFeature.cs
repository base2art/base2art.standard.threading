﻿namespace Base2art.Threading
{
    using System;
    using FluentAssertions;
    using Xunit;

    public class RetryLazyFeature
    {
        [Fact]
        public void ShouldRetryTheLazy()
        {
            var i = -1;
            ILazy<string> lazy = new RetryLazy<string>(
                                                       () =>
                                                       {
                                                           i++;
                                                           if (i < 3)
                                                           {
                                                               throw new InvalidOperationException("Test");
                                                           }

                                                           return i.ToString();
                                                       });

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeFalse();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();

            i = -1;
            lazy.Reset();

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeFalse();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
        }
    }
}