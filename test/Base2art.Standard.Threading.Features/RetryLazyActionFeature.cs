﻿namespace Base2art.Threading
{
    using System;
    using FluentAssertions;
    using Xunit;

    public class RetryLazyActionFeature
    {
        [Fact]
        public void ShouldRetryTheLazy()
        {
            var i = -1;
            IEnsurer lazy = new RetryEnsurer(() =>
            {
                i++;
                if (i < 3)
                {
                    throw new InvalidOperationException("Test");
                }
            });
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Ensured.Should().BeFalse();
            i.Should().Be(2);
            lazy.Ensure();
            lazy.Ensured.Should().BeTrue();
            lazy.Ensure();
            i.Should().Be(3);
            lazy.Ensured.Should().BeTrue();
            i = -1;
            lazy.Reset();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Ensured.Should().BeFalse();
            i.Should().Be(2);
            lazy.Ensure();
            lazy.Ensured.Should().BeTrue();
            i.Should().Be(3);
            lazy.Ensure();
            i.Should().Be(3);
            lazy.Ensured.Should().BeTrue();
        }
    }
}