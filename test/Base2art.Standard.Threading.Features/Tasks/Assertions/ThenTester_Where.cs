﻿namespace Base2art.Threading.Tasks.Assertions
{
    using System.Threading.Tasks;
    using FluentAssertions;
    using Xunit;

    public class ThenTester_Where : ThenTesterBase
    {
        [Fact]
        public async void ShouldDo_Where_NonTask_NonNull()
        {
            (await this.ArrayAsync(1).Then().Where(x => x.Name == "1").Then().FirstOrDefault()).Should().BeNull();
            (await this.ArrayAsync(2).Then().Where(x => x.Name == "1").Then().FirstOrDefault()).Name.Should().Be("1");
        }

        [Fact]
        public async void ShouldDo_Where_NonTask_NonNull_async()
        {
            (await this.ArrayAsync(1).Then().Where(x => Task.FromResult(x.Name == "1")).Then().FirstOrDefault())
                .Should().BeNull();
            (await this.ArrayAsync(2).Then().Where(x => Task.FromResult(x.Name == "1")).Then().FirstOrDefault())
                .Name.Should().Be("1");
        }

        [Fact]
        public async void ShouldDo_Where_NonTask_Null()
        {
            var rez = await this.ArrayAsync(null).Then().Where(x => x.Name == "1");
            rez.Should().BeNull();
        }

        [Fact]
        public async void ShouldDo_Where_NonTask_Null_async()
        {
            var rez = await this.ArrayAsync(null).Then().Where(x => Task.FromResult(x.Name == "1"));
            rez.Should().BeNull();
        }

//        [Fact]
//        public async void ShouldDo_Where_Task_Null()
//        {
//            var rez = await (this.ArrayAsync(null).Then().Where(x => x.Name == "1"));
//            rez.Should().BeNull();
//        }
//
//        [Fact]
//        public async void ShouldDo_Where_Task_NonNull()
//        {
//            (await this.ArrayAsync(2).Then().Where(async x => x.Name == "2").Then().FirstOrDefault()).Name.Should().Be("2");
//        }
    }
}