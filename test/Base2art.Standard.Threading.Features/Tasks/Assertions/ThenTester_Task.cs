﻿namespace Base2art.Threading.Tasks.Assertions
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using FluentAssertions;
    using Xunit;
    using Xunit.Sdk;

    public class ThenTester_Task : ThenTesterBase
    {
        [Fact]
        public async void Should_Execute()
        {
            var count = 0;
            Func<Task> item = async () => { count += 1; };

            Func<Task> Log = async () => { count += 1; };

            var task = item().Then().Execute(Log);

            await task;

            count.Should().Be(2);
        }

        [Fact]
        public async void Should_Execute_Exception()
        {
            var count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
                if (count < 3)
                {
                    throw new Exception();
                }
            };

            Func<Exception, Task> log1 = async x => { count += 1; };

            Action<Exception> log2 = x => { count += 1; };

            var task = item
                .WithEvents()
                .OnError(log1)
                .OnError(log2)
                .OnCompleted(() => { })
                .ExecuteWithRetry(1);
            try
            {
                await task;
            }
            catch (Exception)
            {
                count.Should().Be(3);

                return;
            }

            Assert.True(false, "fail");
        }

        [Fact]
        public async void Should_Execute_NonAsync()
        {
            var count = 0;
            Func<Task> item = async () => { count += 1; };

            Action Log = () => { count += 1; };

            var task = item().Then().Execute(Log);

            await task;

            count.Should().Be(2);
        }

        [Fact]
        public async void Should_ExecuteWithReturn()
        {
            var task = this.ActionAsync().Then().Return(1);
            var data = await task;
            data.Should().Be(1);
        }

        [Fact]
        public async void Should_ExecuteWithReturnTask()
        {
            var task = this.ActionAsync().Then().Return(async () => 1);
            var data = await task;
            data.Should().Be(1);
        }

        [Fact]
        public async void Should_Get()
        {
            var task = this.ListAsync(1).Then().Return(1);
            this.List.Should().BeNull();
            var data = await task;
            this.List.Should().NotBeNull();
            data.Should().Be(1);
        }

        [Fact]
        public async void Should_Get_delegate()
        {
            var task = this.ListAsync(1).Then().Return(() => 2);
            this.List.Should().BeNull();
            var data = await task;
            this.List.Should().NotBeNull();
            data.Should().Be(2);
        }

        [Fact]
        public async void Should_Get_delegateAsync()
        {
            var task = this.ListAsync(1).Then().Return(async () => 2);
            this.List.Should().BeNull();
            var data = await task;
            this.List.Should().NotBeNull();
            data.Should().Be(2);
        }

        [Fact]
        public async void Should_Get_Exception()
        {
            var count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
                if (count < 3)
                {
                    throw new Exception();
                }
            };

            var task = item.WithEvents().ExecuteWithRetry(4);

            await task;
            count.Should().Be(3);
        }

        [Fact]
        public async void Should_Get_Exception_overrun()
        {
            var count = 0;
            Func<Task> item = async () =>
            {
                count += 1;
                if (count < 6)
                {
                    throw new Exception("abc");
                }
            };

            var task = item.WithEvents().ExecuteWithRetry(4);

            try
            {
                await task;
                Assert.True(false, "fail");
            }
            catch (XunitException)
            {
                throw;
            }
            catch (Exception ex)
            {
                ex.Message.Should().Be("abc");
            }

            count.Should().Be(5);
        }

        [Fact]
        public async void Should_Get_Final()
        {
            var count = 0;
            Func<Task> item = () =>
            {
                count += 1;
                if (count < 4)
                {
                    throw new Exception("abc");
                }

                return Task.FromResult(count);
            };

            var task = item.WithEvents().ExecuteWithRetry(3);
            await task;

            count.Should().Be(4);
        }

        [Fact]
        public async void Should_Get_Final_int()
        {
            var count = 0;
            Func<Task<int>> item = () =>
            {
                count += 1;
                if (count < 4)
                {
                    throw new Exception("abc");
                }

                return Task.FromResult(count);
            };

            var task = item.WithEvents().ExecuteWithRetry(3);

            var rez = await task;

            rez.Should().Be(4);
            count.Should().Be(4);
        }

        [Fact]
        public async void Should_ReturnThenExecute()
        {
            var task = this.ListAsync(1).Then().Execute(async () => { await Console.Out.WriteLineAsync(""); });
            await task;
        }

        [Fact]
        public async void TestWrap_Exceptions_Thrown_Retry()
        {
            Func<Task> task = () => Task.Factory.StartNew(() => Console.WriteLine("STARTED"));

            await task.WithEvents()
                      .OnError(ex => Debug.WriteLine(ex))
                      .OnCompleted(() => Console.WriteLine("done"))
                      .ExecuteWithRetry(2);
        }

        [Fact]
        public async void TestWrap_Exceptions_Thrown_Retry_async()
        {
            Func<Task> task = () => Task.Factory.StartNew(() => Console.WriteLine("STARTED"));

            await task.WithEvents()
                      .OnCompleted(() => Task.Factory.StartNew(() => Console.WriteLine("STARTED")))
                      .OnError(ex => Task.Factory.StartNew(() => Debug.WriteLine("STARTED")))
                      .ExecuteWithRetry(2);
        }
    }
}