﻿namespace Base2art.Threading.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class ThenTesterBase
    {
        public ThenTesterBase()
        {
            this.List = null;
        }

        public List<Person> List { get; private set; }

        public async Task ActionAsync()
        {
            await Task.Delay(TimeSpan.FromMilliseconds(50));
        }

        public async Task<List<Person>> ListAsync(int? count)
        {
            if (!count.HasValue)
            {
                return null;
            }

            var list = new List<Person>();
            for (var i = 0; i < count.Value; i++)
            {
                list.Add(new Person {Name = "" + i});
            }

            await Task.Delay(TimeSpan.FromMilliseconds(50));
            this.List = list;
            return list;
        }

        public async Task<Person[]> ArrayAsync(int? count)
        {
            var data = await this.ListAsync(count);
            if (data == null)
            {
                return null;
            }

            return data.ToArray();
        }

        protected async Task<int> MapAsync(int i, Func<int, int> ret)
        {
            return ret(i);
        }

        public class Person
        {
            public string Name { get; set; }
        }
    }
}