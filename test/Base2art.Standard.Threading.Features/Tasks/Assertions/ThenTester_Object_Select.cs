﻿namespace Base2art.Threading.Tasks.Assertions
{
    using System;
    using System.ComponentModel.Design;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using FluentAssertions;
    using Xunit;
    using Xunit.Sdk;

    public class ThenTester_Object_Select : ThenTesterBase
    {
        private Task<string> DataAbc()
        {
            return Task.FromResult("abc");
        }

        private async Task<string> DataAbc(bool throwException)
        {
            if (throwException)
            {
                throw new CheckoutException();
            }

            return await Task.FromResult("abc");
        }

        private Task<string> Data123()
        {
            return Task.FromResult("123");
        }

        public async Task<string[]> GetItemsFromWeb()
        {
            return new[] {"a", "s"};
        }

        [Fact]
        public async void Should_Coalesce()
        {
            var data = await this.ListAsync(1)
                                 .Then()
                                 .FirstOrDefault()
                                 .Then()
                                 .Return(async x => (string) null)
                                 .Then()
                                 .Coalesce(() => " ")
                                 .Then()
                                 .Return(async x => x.Trim());
            data.Should().Be("");
        }

        [Fact]
        public async void Should_Coalesce_Async()
        {
            var data = await this.ListAsync(1)
                                 .Then()
                                 .FirstOrDefault()
                                 .Then()
                                 .Return(async x => (string) null)
                                 .Then()
                                 .Coalesce(async () => " ")
                                 .Then()
                                 .Return(async x => x.Trim());
            data.Should().Be("");
        }

        [Fact]
        public async void Should_CoalescefallThrough()
        {
            var data = await this.ListAsync(1)
                                 .Then()
                                 .FirstOrDefault()
                                 .Then()
                                 .Return(async x => " abc ")
                                 .Then()
                                 .Coalesce(() => " ")
                                 .Then()
                                 .Return(async x => x.Trim());
            data.Should().Be("abc");
        }

        [Fact]
        public async void Should_CoalescefallThrough_Async()
        {
            var data = await this.ListAsync(1)
                                 .Then()
                                 .FirstOrDefault()
                                 .Then()
                                 .Return(async x => " abc ")
                                 .Then()
                                 .Coalesce(async () => " ")
                                 .Then()
                                 .Return(async x => x.Trim());
            data.Should().Be("abc");
        }

        [Fact]
        public async void Should_CoalesceSimple()
        {
            var data = await this.ListAsync(1)
                                 .Then()
                                 .FirstOrDefault()
                                 .Then()
                                 .Return(async x => (string) null)
                                 .Then()
                                 .Coalesce(" ")
                                 .Then()
                                 .Return(async x => x.Trim());
            data.Should().Be("");
        }

        [Fact]
        public async void Should_Get()
        {
            var task = this.ListAsync(1).Then().FirstOrDefault().Then().Return(x => x.Name + "1");
            this.List.Should().BeNull();
            var data = await task;
            data.Should().Be("01");
        }

        [Fact]
        public async void Should_Get_Async()
        {
            var task = this.ListAsync(1).Then().FirstOrDefault().Then().Return(async x => x.Name + "1");
            this.List.Should().BeNull();
            var data = await task;
            data.Should().Be("01");
        }

        [Fact]
        public async void Should_Get_Async_Null()
        {
            var data = await this.ListAsync(1).Then().FirstOrDefault().Then().Return(async x => (string) null).Then()
                                 .Return(async x => x.Trim());
            data.Should().Be(null);
        }

        [Fact]
        public async void Should_Get_Exception()
        {
            var count = 0;
            Func<Task<int>> item = () =>
            {
                count += 1;
                if (count < 3)
                {
                    throw new Exception();
                }

                return Task.FromResult(count);
            };

            var task = item.WithEvents().ExecuteWithRetry(4);

            var counti = await task;
            count.Should().Be(3);
            counti.Should().Be(3);
        }

        [Fact]
        public async void Should_Get_Exception_overrun()
        {
            var count = 0;
            Func<Task<int>> item = () =>
            {
                count += 1;
                if (count < 6)
                {
                    throw new Exception("abc");
                }

                return Task.FromResult(count);
            };

            var task = item.WithEvents().ExecuteWithRetry(4);

            try
            {
                await task;
                Assert.True(false, "Fail");
            }
            catch (XunitException ae)
            {
                throw;
            }
            catch (Exception ex)
            {
                ex.Message.Should().Be("abc");
            }

            count.Should().Be(5);
        }

        [Fact]
        public async void Should_Get_Null()
        {
            var data = await this.ListAsync(1).Then().FirstOrDefault().Then().Return(x => (string) null).Then()
                                 .Return(x => x.Trim());
            data.Should().Be(null);
        }

        [Fact]
        public async void TestWrap()
        {
            var result = await Task.FromResult("abc").Then().Return(async x => x.Length).Then().Return(x => x * 2);
            result.Should().Be(6);
            var result1 = await Task.FromResult(true).Then().Return(async x => !x);
            result1.Should().BeFalse();
        }

        [Fact]
        public async void TestWrap_Basic()
        {
            Task task = Task.FromResult("abc");
            var result = await task.Then().Return(() => 4);
            result.Should().Be(4);
        }

        [Fact]
        public async void TestWrap_Exceptions_NoneThrown()
        {
            var writer = new StringWriter();

            Func<Task<string>> task = () => this.DataAbc();
            var result = await task.WithEvents().OnCompleted(a => this.Data123()).Execute();

            result.Should().Be("abc");
            writer.Flush();
            writer.GetStringBuilder().ToString().Should().Be("");

            Func<Task<string>> task1 = () => this.DataAbc();
            var result1 = await task1.WithEvents()
                                     .OnError(x => this.Data123()
                                                       .Then()
                                                       .Execute(y => writer.WriteLine(y)))
                                     .Execute();

            result1.Should().Be("abc");
            writer.Flush();

            writer.GetStringBuilder().ToString().Should().Be("");
        }

        [Fact]
        public async void TestWrap_Exceptions_NoneThrown_Log()
        {
            Func<Task<string>> task = () => this.DataAbc();
            Action<string> log = Console.WriteLine;
            var result = await task.WithEvents().OnError(a => Console.WriteLine("sdf")).Execute();
            result.Should().Be("abc");

            Func<Task<string>> task1 = () => this.DataAbc();
            var result1 = await task1.WithEvents().OnError(x => Console.WriteLine("sdf")).Execute();
            result1.Should().Be("abc");
        }

        [Fact]
        public async void TestWrap_Exceptions_NoneThrown_Retry()
        {
            Func<Task<string>> task = async () => await this.DataAbc();

            var writer = new StringWriter();
            var result = await task.WithEvents()
                                   .OnError(ex => writer.WriteLine("asd"))
                                   .OnCompleted(pre => writer.Write(pre + "Done"))
                                   .ExecuteWithRetry(2);

            result.Should().Be("abc");
            writer.GetStringBuilder().ToString().Should().Be("abcDone");
        }

        [Fact]
        public async void TestWrap_Exceptions_NoneThrown_Retry_async()
        {
            Func<Task<string>> task = async () => await this.DataAbc();

            var writer = new StringWriter();
            var result = await task.WithEvents()
                                   .OnError(async ex => writer.Write(await this.Data123()))
                                   .OnCompleted(async pre => await writer.WriteAsync(pre + "Done"))
                                   .ExecuteWithRetry(2);

            result.Should().Be("abc");
            writer.GetStringBuilder().ToString().Should().Be("abcDone");
        }

        [Fact]
        public async void TestWrap_Exceptions_SimpleApiTest()
        {
//            Func<Task<string>> task = async () => await this.DataAbc();
            var result = await this.GetItemsFromWeb()
                                   .WithEvents()
                                   .OnCompleted(x => Console.WriteLine(x.ToString()))
                                   .OnError(x => Debug.WriteLine(x.ToString()))
                                   .Execute()
                                   .Then()
                                   .Select(x => x + "-Suffix")
                                   .Then().ToList();
            result.Count().Should().Be(2);
            result[0].Should().Be("a-Suffix");
        }

        [Fact]
        public async void TestWrap_Exceptions_Thrown()
        {
            Func<Task<string>> task = async () =>
            {
                throw new Exception("");
                return await this.DataAbc();
            };

            var writer = new StringWriter();

            await Assert.ThrowsAsync<Exception>(() => task.WithEvents()
                                                          .OnError(async ex => writer.Write(await this.Data123()))
                                                          .Execute());

            writer.GetStringBuilder().ToString().Should().Be("123");

            Func<Task<string>> task1 = async () =>
            {
                throw new Exception("");
                return await this.DataAbc();
            };

            writer = new StringWriter();
            await Assert.ThrowsAsync<Exception>(() => task1.WithEvents().OnError(ex => writer.Write("123")).Execute());

//            var result1 = await task1.WithEvents().OnError(ex => Console.WriteLine("123")).Execute();
            writer.GetStringBuilder().ToString().Should().Be("123");
        }

        [Fact]
        public async void TestWrap_Exceptions_Thrown_Retry()
        {
            var i = 0;
            Func<Task<string>> task = async () =>
            {
                i += 1;
                return await this.DataAbc(i < 2);
            };

            var writer = new StringWriter();
            var result = await task.WithEvents()
                                   .OnError(ex => writer.WriteLine("asd"))
                                   .OnCompleted(pre => writer.Write(pre + "Done"))
                                   .ExecuteWithRetry(2);

            result.Should().Be("abc");
            writer.GetStringBuilder().ToString().Should().Be("abcDone");
        }

        [Fact]
        public async void TestWrap_Exceptions_Thrown_Retry_async()
        {
            var i = 0;
            Func<Task<string>> task = async () =>
            {
                i += 1;
                return await this.DataAbc(i < 2);
            };

            var writer = new StringWriter();
            var result = await task.WithEvents()
                                   .OnError(async ex => writer.Write(await this.Data123()))
                                   .OnCompleted(async pre => await writer.WriteAsync(pre + "Done"))
                                   .ExecuteWithRetry(2);

            result.Should().Be("abc");
            writer.GetStringBuilder().ToString().Should().Be("abcDone");
        }
    }
}