﻿namespace Base2art.Threading.Tasks.Api
{
    using System.Threading.Tasks;
    using Xunit;

    public class CollectionApiTest
    {
        [Fact]
        public async void FirstOrDefault()
        {
            await SampleTasks.Coll()
                             .Then()
                             .FirstOrDefault();
        }

        [Fact]
        public async void Select()
        {
            var data = await SampleTasks.Coll()
                                        .Then()
                                        .Select(x => "a" + x)
                                        .Then()
                                        .ToArray();
        }

        [Fact]
        public async void Select_Async()
        {
            var data = await SampleTasks.Coll()
                                        .Then()
                                        .Select(x => Task.FromResult("a" + x))
                                        .Then()
                                        .ToArray();
        }

        [Fact]
        public async void ToArray()
        {
            var data = await SampleTasks.Coll()
                                        .Then()
                                        .Select(x => Task.FromResult("a" + x))
                                        .Then()
                                        .ToArray();
        }

        [Fact]
        public async void ToList()
        {
            var data = await SampleTasks.Coll()
                                        .Then()
                                        .Select(x => "a" + x)
                                        .Then()
                                        .ToList();
        }

        [Fact]
        public async void Where()
        {
            await SampleTasks.Coll()
                             .Then()
                             .Where(x => x == "A")
                             .Then()
                             .ToArray();
        }

        [Fact]
        public async void Where_Async()
        {
            await SampleTasks.Coll()
                             .Then()
                             .Where(x => Task.FromResult(x == "A"))
                             .Then()
                             .ToArray();
        }
    }
}