﻿namespace Base2art.Threading.Tasks.Api
{
    using Xunit;

    public class ObjectApiTest
    {
        [Fact]
        public async void Coalesce_Action()
        {
            await SampleTasks.Null()
                             .Then()
                             .Coalesce(() => "B");
        }

        [Fact]
        public async void Coalesce_Async()
        {
            await SampleTasks.Null()
                             .Then()
                             .Coalesce(() => SampleTasks.B());
        }

        [Fact]
        public async void Coalesce_Value()
        {
            await SampleTasks.Null()
                             .Then()
                             .Coalesce("B");
        }

        [Fact]
        public async void Execute_Action()
        {
            await SampleTasks.A()
                             .Then()
                             .Execute(() => SampleTasks.Log());
        }

        [Fact]
        public async void Execute_Async()
        {
            await SampleTasks.A()
                             .Then()
                             .Execute(async () => await SampleTasks.LogAsync());
        }

        [Fact]
        public async void Return_Action()
        {
            await SampleTasks.A()
                             .Then()
                             .Return(a => "B");
        }

        [Fact]
        public async void Return_Async()
        {
            await SampleTasks.A()
                             .Then()
                             .Return(a => SampleTasks.B());
        }
    }
}