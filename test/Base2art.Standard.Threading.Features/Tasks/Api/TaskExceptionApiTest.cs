﻿namespace Base2art.Threading.Tasks.Api
{
    using System;
    using System.Diagnostics;
    using System.Threading.Tasks;
    using Xunit;

    public class TaskExceptionApiTest
    {
        [Fact]
        public async void When_Exception_Action()
        {
            Func<Task> aProducer = SampleTasks.A;

            await aProducer.WithEvents()
                           .OnError(x => Debug.WriteLine(x))
                           .OnCompleted(() => Console.WriteLine("Done"))
                           .Execute();
        }

        [Fact]
        public async void When_Exception_Async()
        {
            Func<Task> aProducer = SampleTasks.A;

            await aProducer.WithEvents()
                           .OnError(async x => await Task.Factory.StartNew(() => Debug.WriteLine(x)))
                           .OnCompleted(async () => await Task.Factory.StartNew(() => Console.WriteLine("Done")))
                           .Execute();
        }
    }
}