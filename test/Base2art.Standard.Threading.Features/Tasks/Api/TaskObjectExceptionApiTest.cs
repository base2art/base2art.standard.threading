﻿namespace Base2art.Threading.Tasks.Api
{
    using System;
    using System.Threading.Tasks;
    using Xunit;

    public class TaskObjectExceptionApiTest
    {
        [Fact]
        public async void When_Exception_Action()
        {
            Func<Task<string>> aProducer = SampleTasks.A;

            await aProducer.WithEvents()
                           .OnError(x => Console.WriteLine("B"))
                           .OnCompleted(a => Console.WriteLine("C"))
                           .Execute();
        }

        [Fact]
        public async void When_Exception_Async()
        {
            Func<Task<string>> aProducer = SampleTasks.A;

            await aProducer.WithEvents()
                           .OnError(async x => await SampleTasks.B())
                           .OnCompleted(async a => await SampleTasks.B())
                           .Execute();
        }
    }
}