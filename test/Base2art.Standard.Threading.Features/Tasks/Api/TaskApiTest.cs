﻿namespace Base2art.Threading.Tasks.Api
{
    using Xunit;

    public class TaskApiTest
    {
        [Fact]
        public async void Execute_Action()
        {
            await SampleTasks.A()
                             .Then()
                             .Execute(() => SampleTasks.Log());
        }

        [Fact]
        public async void Execute_Async()
        {
            await SampleTasks.A()
                             .Then()
                             .Execute(async () => await SampleTasks.LogAsync());
        }

        [Fact]
        public async void Return_Action()
        {
            await SampleTasks.A()
                             .Then()
                             .Return(() => "B");
        }

        [Fact]
        public async void Return_Async()
        {
            await SampleTasks.A()
                             .Then()
                             .Return(SampleTasks.B);
        }

        [Fact]
        public async void Return_Value()
        {
            await SampleTasks.A()
                             .Then()
                             .Return("B");
        }
    }
}