﻿namespace Base2art.Threading.Tasks
{
    using System.Threading.Tasks;

    public static class SampleTasks
    {
        public static Task<string> A()
        {
            return Task.FromResult("A");
        }

        public static Task<string> B()
        {
            return Task.FromResult("B");
        }

        public static Task<string> Null()
        {
            return Task.FromResult<string>(null);
        }

        public static Task<string[]> Coll()
        {
            return Task.FromResult(new[] {"A", "B"});
        }

        public static void Log()
        {
        }

        public static Task LogAsync()
        {
            return Task.FromResult(true);
        }
    }
}