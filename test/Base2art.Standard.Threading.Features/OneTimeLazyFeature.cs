namespace Base2art.Threading
{
    using System;
    using FluentAssertions;
    using Xunit;

    public class OneTryLazyFeature
    {
        [Fact]
        public void ShouldRetryTheLazy()
        {
            var i = -1;
            ILazy<string> lazy = new OneTryLazy<string>(
                                                        () =>
                                                        {
                                                            i++;
                                                            if (i < 3)
                                                            {
                                                                throw new InvalidOperationException("Test");
                                                            }

                                                            return i.ToString();
                                                        });

            lazy.IsValueCreated.Should().BeFalse();

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeTrue();

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();

            lazy.Reset();

            lazy.IsValueCreated.Should().BeFalse();

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            lazy.IsValueCreated.Should().BeTrue();

            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();
            new Action(() => new Func<string>(() => lazy.Value).Invoke())
                .ShouldThrow<InvalidOperationException>();

            lazy.Reset();
            i = 2;
            lazy.Value.Should().Be("3");
            lazy.IsValueCreated.Should().BeTrue();
        }
    }
}