﻿namespace Base2art.Threading
{
    using System;
    using FluentAssertions;
    using Xunit;

    public class OneTryEnsurerFeature
    {
        [Fact]
        public void ShouldRetryTheEnsurer()
        {
            var i = -1;
            IEnsurer lazy = new OneTryEnsurer(() =>
            {
                i++;
                if (i < 3)
                {
                    throw new InvalidOperationException("Test");
                }
            });
            lazy.Ensured.Should().BeFalse();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Ensured.Should().BeTrue();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Reset();
            lazy.Ensured.Should().BeFalse();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Ensured.Should().BeTrue();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            new Action(lazy.Ensure).ShouldThrow<InvalidOperationException>();
            lazy.Reset();
            i = 2;
            lazy.Ensure();
            i.Should().Be(3);
            lazy.Ensured.Should().BeTrue();
        }
    }
}